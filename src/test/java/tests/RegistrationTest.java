package tests;

import config.TestConfig;
import org.junit.Test;
import pages.HomePage;

public class RegistrationTest extends TestConfig {

    @Test
    public void userShouldSuccessfullyRegister() {
        new HomePage()
                .openSignInPage()
                .submitCreateAccountFormWithValidEmail()
                .signUpWithValidData()
                .userShouldBeSuccessfullyRegistered();
    }

    @Test
    public void registrationWithInvalidDataShouldFail() {
        new HomePage()
                .openSignInPage()
                .submitCreateAccountFormWithValidEmail()
                .signUpWithInvalidData()
                .userShouldSeeRegistrationFormAlert();
    }
}