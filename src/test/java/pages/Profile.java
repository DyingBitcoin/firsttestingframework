package pages;

import io.qameta.allure.Step;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Profile extends BasePage{

    public Profile() { super(); }

    @FindBy(css = "#center_column > h1")
    private WebElement profilePageHeader;

    @Step
    public void userShouldBeSuccessfullyRegistered() {
        String profilePageHeaderText = profilePageHeader.getText();
        String expectedHeaderText = "MY ACCOUNT";

        MatcherAssert.assertThat(profilePageHeaderText, IsEqual.equalTo(expectedHeaderText));
    }
}
