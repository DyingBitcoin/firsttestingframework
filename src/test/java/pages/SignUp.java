package pages;

import io.qameta.allure.Step;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsCollectionContaining;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import utility.DataFaker;

import java.util.ArrayList;
import java.util.List;

import static utility.Screenshot.captureScreenshot;

public class SignUp extends BasePage{

    public SignUp() { super(); };

    private DataFaker faker = new DataFaker();

    @FindBy(id = "id_gender1")
    private WebElement maleRadioButton;

    @FindBy(id = "id_gender2")
    private WebElement femaleRadioButton;

    @FindBy(id = "customer_firstname")
    private WebElement customerFirstNameInput;

    @FindBy(id = "customer_lastname")
    private WebElement customerLastNameInput;

    @FindBy(id = "passwd")
    private WebElement customerPasswordInput;

    @FindBy(id = "days")
    private WebElement dayOfBirthSelect;

    @FindBy(id = "months")
    private WebElement monthOfBirthSelect;

    @FindBy(id = "years")
    private WebElement yearOfBirthSelect;

    @FindBy(id = "address1")
    private WebElement addressInput;

    @FindBy(id = "city")
    private WebElement cityInput;

    @FindBy(id = "id_state")
    private WebElement stateSelect;

    @FindBy(id = "postcode")
    private WebElement postcodeInput;

    @FindBy(id = "phone_mobile")
    private WebElement mobilePhoneNumberInput;

    @FindBy(id = "submitAccount")
    private WebElement registerButton;

    @FindBy(css = "#center_column > .alert li")
    private List<WebElement> alertMessageContent;

    public void fillingYourPersonalInformationForm(boolean validForm) {
        maleRadioButton.click();
        if (validForm) {
            customerFirstNameInput.sendKeys(faker.getFakeFirstname());
        }
        customerLastNameInput.sendKeys(faker.getFakeLastname());
        customerPasswordInput.sendKeys(faker.getFakePassword());
        new Select(dayOfBirthSelect).selectByValue("4");
        new Select(monthOfBirthSelect).selectByValue("10");
        new Select(yearOfBirthSelect).selectByValue("1999");
        addressInput.sendKeys(faker.getFakeStreet());
        cityInput.sendKeys(faker.getFakeCity());
        new Select(stateSelect).selectByValue("4");
        postcodeInput.sendKeys("11234");
        mobilePhoneNumberInput.sendKeys(faker.getFakeMobilePhone());
    }

    @Step
    public Profile signUpWithValidData(){
        fillingYourPersonalInformationForm(true);
        captureScreenshot();
        registerButton.click();
        return new Profile();
    }

    @Step
    public SignUp signUpWithInvalidData(){
        fillingYourPersonalInformationForm(false);
        captureScreenshot();
        registerButton.click();
        return new SignUp();
    }

    @Step
    public void userShouldSeeRegistrationFormAlert() {
        String EXPECTED_MESSAGE = "firstname is required.";
        MatcherAssert.assertThat(getAlertMessageContent(), IsCollectionContaining.hasItem(EXPECTED_MESSAGE));
    }

    private List<String> getAlertMessageContent() {
        List<String> alertMessages = new ArrayList<String>();

        for(WebElement message : alertMessageContent) {
            alertMessages.add(message.getText());
        }
        System.out.println(alertMessages);
        return alertMessages;
    }
}
